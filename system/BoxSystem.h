//
// Created by LiaoJiaPeng on 19-11-28.
//

#ifndef ENGINEERPRO_BOXSYSTEM_H
#define ENGINEERPRO_BOXSYSTEM_H
#include "../common/CycleQueue.h"
#include "../common/Logger.h"
#include "../driver/SerialPort.h"
#include "../driver/BoxCamera.h"
#include "../perception/BoxDetection.h"
#include <signal.h>

class BoxSystem {
public:
    BoxSystem(std::string cameraName);

    ~BoxSystem();

    bool init();

    void producer();

    void consumer();

private:
    static void signalHandler(int);

    void initSignal();

private:

    SerialPort            _serial;
    BoxCamera             _Bcamera;    //boxcamera
    CycleQueue<cv::Mat>   _imageBuffer;
    BoxDetection          _Bdetector;
    static bool           _quitFlag;


};


#endif //ENGINEERPRO_BOXSYSTEM_H
