//
// Created by LiaoJiaPeng on 19-11-28.
//

#include "BoxSystem.h"
#include <chrono>

bool BoxSystem::_quitFlag;

BoxSystem::BoxSystem(std::string cameraName):_Bcamera(cameraName),
                                             _imageBuffer(1)
{
}

BoxSystem::~BoxSystem()
{
}



void BoxSystem::producer()
{
    VideoCapture cap(0);
    Mat frame;

    for(;;)
    {
        if(_quitFlag)
        {
            return;
        }
        cap>>frame;
        _imageBuffer.push(frame.clone());
        cout<<"producer"<<endl;
    }
}

void BoxSystem::consumer()
{

    vector<float> coordination;
    for(;;)
    {
    if(_quitFlag)
    {
        return;
    }
    cv::Mat img;
    _imageBuffer.pop(img);
    int count;


    //auto start = chrono::system_clock::now();
    //imshow("img",img);

    if(img.empty())
    {
            cout<<"img is empty"<<endl;
            continue;
    }
    else
    {

        coordination = _Bdetector.autoAlignment(img);
        _Bdetector.showBoxImage(img, false);

        //auto end   = chrono::system_clock::now();
        //auto duration = chrono::duration_cast<std::chrono::microseconds>(end - start);
        //cout<<double(duration.count()) * chrono::microseconds::period::num / chrono::microseconds::period::den  *1000 << "ms" << endl;
        waitKey(1);
    }
    cout<<"consumer"<<endl;
    }

}

