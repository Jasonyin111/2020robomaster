//
// Created by LiaoJiaPeng on 19-11-28.
//

#ifndef ENGINEERPRO_BOXDETECTION_H
#define ENGINEERPRO_BOXDETECTION_H
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include "../common/GetMean.h"
#include "../common/CalculateForBox.h"

class BoxDetection : public CalculateForBox {
public:
    BoxDetection();

    ~BoxDetection();

    bool showBoxImage(Mat frame, bool windowed = true, Size screenSize = Size(800, 480));

    vector<float> autoAlignment(Mat &frame);

    bool movementDetection();

private:
    Mat imageShow;

    Mat Mask;

    Mat imageROI;

    int frameNum = 0;

    vector<Vec3f> calculateCircles;

    float lastPixelX = 0;

    float nextPixelX = 0;




};


#endif //ENGINEERPRO_BOXDETECTION_H
