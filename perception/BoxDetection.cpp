//
// Created by LiaoJiaPeng on 19-11-28.
//

#include "BoxDetection.h"

#define MAX_MOVE_PIXELX 20

#define PIXEL_PROPORATION 37


BoxDetection::BoxDetection()
{
    imageShow = Mat::zeros(Size(800, 480), CV_8UC3);
    Mask = Mat::ones(Size(640, 480), CV_8UC1);
    imageROI = imageShow(Rect(60, 0, 640, 480));
}

BoxDetection::~BoxDetection() {}

bool BoxDetection::showBoxImage(Mat frame, bool windowed, Size screenSize)
{
    resize(frame, frame, Size(640, 480));
    frame.copyTo(imageROI, Mask);
    if(windowed == false)
    {
        namedWindow("boxShow", WINDOW_AUTOSIZE);
        //setWindowProperty("boxShow", WND_PROP_FULLSCREEN, WINDOW_FULLSCREEN);
    }
    imshow("boxShow",imageShow);
    return true;
}

vector<float> BoxDetection::autoAlignment(Mat &frame)
{
    Mat grayFrame;
    Mat kernel = (Mat_<float>(3, 3) << 0, -1, 0, -1, 5, -1, 0, -1, 0); //拉普拉斯算子
    Ptr<CLAHE> clahe = createCLAHE();    //自适应直方图均衡
    clahe->setClipLimit(4);				//设置对比度阀值
    vector<float> coordination;
    cvtColor(frame, grayFrame, COLOR_BGR2GRAY);				//将获取的图像并对其进行高斯滤波
    GaussianBlur(grayFrame, grayFrame, Size(7, 7), 2, 2);
    clahe->apply(grayFrame,grayFrame);//应用自适应
    vector<Vec3f> circles;
    //imshow("gray", grayFrame);
    HoughCircles(grayFrame, circles, HOUGH_GRADIENT, 1, 70, 100, 100, 100, 500);		//霍夫圆检测
    //在calculaterCircles末尾插入circles的所有元素
    calculateCircles.insert(calculateCircles.end(), circles.begin(), circles.end());
//        cout<<calculateCircles.size()<<endl;

    if(calculateCircles.size() == 0)
    {
        lastPixelX = nextPixelX;
        nextPixelX = 0;
    }
    else
    {
        Vec3f meanCircle = getMeans(calculateCircles); //取多个圆的平均
        Point center(cvRound(meanCircle[0]), cvRound(meanCircle[1]));
        int radius = cvRound(meanCircle[2]);
        circle(frame, center, radius, Scalar(0, 255, 0), 3); //此处画圆


        double temp=0;


        static GetMean q(5);//不加static 每次都会初始化 无效果
        temp=q.next(center.x);
        cout<<"origin:"<<endl;
        cout<<center.x<<endl;
        cout<<"result:"<<endl;
        cout<<(int)temp<<endl;


        //imshow("aini",frame);
        //此两条线为圆内十字架
        line(frame, Point(center.x - radius, center.y), Point(center.x + radius, center.y), Scalar(0, 0, 255), 3);
        line(frame, Point(center.x, center.y - radius), Point(center.x, center.y + radius), Scalar(0, 0, 255), 3);
        //水平偏差量
        float pixelX = center.x - frame.cols / 2;
        lastPixelX = nextPixelX;
        nextPixelX = pixelX;
        /*------------------------------------------------------------*/
        float distanceY = pixelX / PIXEL_PROPORATION;
        float f[3] = {0, -distanceY/float(100)*float(1.04), 0};
        coordination.push_back(f[0]);
        coordination.push_back(f[1]);
        coordination.push_back(f[2]);
        calculateCircles.clear();
    }
    //此为整个屏幕的十字架
    line(frame, Point(frame.cols / 2, 0), Point(frame.cols / 2, frame.rows), Scalar(0, 0, 255), 5);
    line(frame, Point(0, frame.rows / 2), Point(frame.cols, frame.rows / 2), Scalar(0, 0, 255), 5);
    return coordination;
}

bool BoxDetection::movementDetection()
{
    if(abs(nextPixelX - lastPixelX) > MAX_MOVE_PIXELX || lastPixelX == 0 || nextPixelX == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}