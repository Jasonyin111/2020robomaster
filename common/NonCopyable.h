//
// Created by dl1ja12 on 19-11-28.
//

#ifndef ENGINEERPRO_NONCOPYABLE_H
#define ENGINEERPRO_NONCOPYABLE_H

#endif //ENGINEERPRO_NONCOPYABLE_H
namespace _non_copyable
{
    class NonCopyable
    {
    protected:
        NonCopyable() = default;
        virtual ~NonCopyable() = default;
    private:
        NonCopyable(const NonCopyable&) = delete;
        const NonCopyable& operator=(const NonCopyable&) = delete;
    };
} // _non_copyable

namespace _non_moveable
{
    class NonMoveable
    {
    protected:
        NonMoveable() = default;
        virtual ~NonMoveable() = default;
    private:
        NonMoveable(NonMoveable&&) = delete;
        const NonMoveable& operator=(NonMoveable&&) = delete;
    };
} // _non_moveable

namespace _non_constructible
{
    class NonConstructible
    {
    private:
        NonConstructible() = default;
    protected:
        virtual ~NonConstructible();
    };
} // _non_constructiable

typedef _non_copyable::NonCopyable NonCopyable;
typedef _non_moveable::NonMoveable NonMoveable;
typedef _non_constructible::NonConstructible NonConstructible;