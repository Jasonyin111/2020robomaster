//
// Created by dl1ja12 on 19-11-28.
//

#include "CalculateForBox.h"
CalculateForBox::CalculateForBox() {}

CalculateForBox::~CalculateForBox() {}

double CalculateForBox::getDistance(Point pointO, Point pointA)
{
    double distance;
    distance = powf((pointO.x - pointA.x), 2) + powf((pointO.y - pointA.y), 2);
    distance = sqrtf(distance);
    return distance;
}

Vec3f CalculateForBox::getMeans(std::vector<Vec3f> cricles)
{
    if(cricles.size() == 0)
    {
        return Point3f(0, 0, 0);
    }
    float meansX = 0;
    float meansY = 0;
    float meansR = 0;
    for (int i = 0; i < cricles.size(); i++)
    {
        meansX += cricles[i][0];
        meansY += cricles[i][1];
        meansR += cricles[i][2];
    }
    Point3f meansCricle = Point3f(meansX/cricles.size(), meansY/cricles.size(), meansR/cricles.size());
    return meansCricle;
}

float CalculateForBox::getK(Vec4i line)
{
    float K = 0;
    if (line[0] == line[2])
    {
        K = INT_MAX;
    }
    else if (line[1] == line[3])
    {
        K = 0;
    }
    else
    {
        K = (float(line[3]) - float(line[1])) /
            (float(line[2]) - float(line[0]));
    }
    return K;
}

Point2f CalculateForBox::cpmputeSetCenter(vector<Point2f> pointSet)
{
    Point2f SetCenter = Point2f(0, 0);
    float sumX = 0;
    float sumY = 0;
    for (int i = 0; i < pointSet.size(); i++)
    {
        sumX += pointSet[i].x;
        sumY += pointSet[i].y;
    }
    SetCenter = Point2f(sumX / pointSet.size(), sumY / pointSet.size());
    return SetCenter;
}
